//
//  OTMatrixGenerator.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTMatrixGenerator.h"
#import "NSString+RandomChar.h"
#import "AppDelegate.h"

@interface OTMatrixGenerator()

@property int m;
@property int n;
@property (nonatomic, strong) NSString *charSet;
@property (nonatomic, strong) NSMutableArray *matrix;

@end

@implementation OTMatrixGenerator

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.matrix = [NSMutableArray new];
		self.charSet = @"abcdefghijklmnopqrstuvwxyz1234567890";
	}
	return self;
}

-(NSMutableArray*)getMatrixWithM:(int)m
							andN:(int)n {
	self.m = m;
	self.n = n;
	[self generateMatrix];
	return self.matrix;
}

-(void)generateMatrix {
	for (int i = 0; i < self.m; i++) {
		[self.matrix addObject:[NSMutableArray new]];
		for (int j = 0; j < self.n; j++) {
			NSString *c = [NSString randomStringFromCharacters:self.charSet];
			[self.matrix[i] addObject:c];
		}
	}
}

@end
