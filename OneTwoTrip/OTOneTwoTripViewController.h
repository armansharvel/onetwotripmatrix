//
//  OTOneTwoTripViewController.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 21/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTViewController.h"

@interface OTOneTwoTripViewController : OTViewController

@property (nonatomic, strong) NSMutableDictionary *oneTwoTripDict;

@end
