//
//  NSString+RandomChar.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "NSString+RandomChar.h"

@implementation NSString (RandomChar)

+ (NSString *)randomStringFromCharacters:(NSString *)chars
{
	unichar str[1];
	str[0] = [chars characterAtIndex:arc4random() % [chars length]];
	
	return [NSString stringWithCharacters:str length:1];
}

@end
