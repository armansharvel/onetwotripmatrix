//
//  OTOneTwoTripCell.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 21/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTOneTwoTripCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *letterLabel;
@property (weak, nonatomic) IBOutlet UILabel *mLabel;
@property (weak, nonatomic) IBOutlet UILabel *nLabel;

@end
