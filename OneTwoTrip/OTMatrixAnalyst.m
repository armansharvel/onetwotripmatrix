//
//  OTMatrixAnalyst.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTMatrixAnalyst.h"

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

@interface OTMatrixAnalyst()

@property (nonatomic, strong) NSMutableArray *matrix;
@property (nonatomic, strong) NSMutableArray *wantedWord;

@property (nonatomic, strong) NSMutableArray *finalLetterSet;
@property (nonatomic, strong) NSMutableArray *finalMSet;
@property (nonatomic, strong) NSMutableArray *finalNSet;
@property (nonatomic, strong) NSMutableDictionary *finalOneTwoTripDict;

@property int iterationNumber;

@property int m;
@property int n;
@property int amountOfCharsSet;

@end

@implementation OTMatrixAnalyst

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.matrix = [NSMutableArray new];
		self.wantedWord = [NSMutableArray arrayWithObjects:@"o",@"n",@"e",@"t",@"w",@"o",@"t",@"r",@"i",@"p", nil];
		self.finalLetterSet = [NSMutableArray arrayWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
		self.finalMSet = [NSMutableArray arrayWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
		self.finalNSet = [NSMutableArray arrayWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
		self.finalOneTwoTripDict = [NSMutableDictionary new];
		self.amountOfCharsSet = 0;
		
		self.iterationNumber = 0;
	}
	return self;
}

-(NSMutableDictionary*)getFinalOneTwoTripDictFromMatrix:(NSMutableArray*)matrix {
	
	self.matrix = matrix;
	self.m = (int)self.matrix.count;
	self.n = (int)[self.matrix[0] count];
	[self analyzeMatrix];
	
	for (NSString*string in self.finalLetterSet) {
		NSLog(@"%@", string);
	}
	return [self setupFinalOneTwoTripDict];
}

-(void)analyzeMatrix {
	
	// ALGORITHM BEGINS
	TICK;
	
	for (int i = 0; i < self.m; i++) {
		for (int j = 0; j < self.n; j++) {
			
			[self setCharFromM:i
						  andN:j];
			
			if (self.amountOfCharsSet == 10) {
				
				TOCK;
				// ALGORITHM ENDS
				
				return;
			}
		}
	}
	NSLog(@"%@", self.finalLetterSet);
	self.finalLetterSet = nil;
	NSLog(@"Impossible");
}

-(void)setCharFromM:(int)mIdx andN:(int)nIdx {
	
	for (int i = 0; i < self.wantedWord.count; i++) {
		
		self.iterationNumber++;
		NSLog(@"Iteration number: %i", self.iterationNumber);
		
		if ([self.matrix[mIdx][nIdx] isEqualToString:self.wantedWord[i]] &&
			[self.finalLetterSet[i] isEqualToString:@""]) {
			
			[self.finalLetterSet
			 setObject:[NSString stringWithFormat:@"%@",self.matrix[mIdx][nIdx]]
			 atIndexedSubscript:i];
			
			[self.finalMSet
			 setObject:[NSString stringWithFormat:@"%i",mIdx]
			 atIndexedSubscript:i];
			
			[self.finalNSet
			 setObject:[NSString stringWithFormat:@"%i",nIdx]
			 atIndexedSubscript:i];
			
			self.amountOfCharsSet++;
			
			return;
		}
	}
}

-(NSMutableDictionary*)setupFinalOneTwoTripDict {
	
	if (self.finalLetterSet != nil) {
		[self.finalOneTwoTripDict setObject:self.finalLetterSet forKey:@"letterSet"];
		[self.finalOneTwoTripDict setObject:self.finalMSet forKey:@"mSet"];
		[self.finalOneTwoTripDict setObject:self.finalNSet forKey:@"nSet"];
		
		return self.finalOneTwoTripDict;
	}
	return nil;
}


@end
