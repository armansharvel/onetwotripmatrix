//
//  OTOneTwoTripViewController.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 21/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTOneTwoTripViewController.h"
#import "OTOneTwoTripCell.h"

@interface OTOneTwoTripViewController () <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *letterStrings;
@property (nonatomic, strong) NSArray *mStrings;
@property (nonatomic, strong) NSArray *nStrings;

@end

@implementation OTOneTwoTripViewController

#pragma mark - UIViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	[self setupData];
	[self setupTableView];
}

#pragma mark - Initialization

-(void)setupData {
	self.letterStrings = self.oneTwoTripDict[@"letterSet"];
	self.mStrings = self.oneTwoTripDict[@"mSet"];
	self.nStrings = self.oneTwoTripDict[@"nSet"];
}

-(void)setupTableView {
	self.tableView.dataSource = self;
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section {
	
	return self.letterStrings.count;
}

-(OTOneTwoTripCell*)tableView:(UITableView *)tableView
		cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	OTOneTwoTripCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OTOneTwoTripCell"
															 forIndexPath:indexPath];
	
	cell.letterLabel.text = [self.letterStrings[indexPath.row] uppercaseString];
	cell.mLabel.text = self.mStrings[indexPath.row];
	cell.nLabel.text = self.nStrings[indexPath.row];
	
	return cell;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

@end
