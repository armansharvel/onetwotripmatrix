//
//  OTViewController.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 21/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTViewController.h"

@interface OTViewController ()

@end

@implementation OTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
	return  UIStatusBarStyleLightContent;
}

-(void)layoutIfNeededWithDuration:(NSTimeInterval)duration {
	[UIView animateWithDuration:duration animations:^{
		[self.view layoutIfNeeded];
	}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
