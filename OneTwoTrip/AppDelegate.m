//
//  AppDelegate.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 19/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "AppDelegate.h"
#import "OTActivityIndicator.h"

@interface AppDelegate ()

@property (strong, nonatomic) OTActivityIndicator *activity;
@property (strong, nonatomic) UIView *activity_back;

@end

@implementation AppDelegate

+ (AppDelegate *)shared
{
	return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Override point for customization after application launch.
	return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - activity

- (void)showActivity
{
	if ([NSThread isMainThread])
	{
		[self.window addSubview:self.activity];
		[self.activity startAnimating];
	}
	else
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.window addSubview:self.activity];
			[self.activity startAnimating];
		});
	}
}

- (void)hideActivity
{
	if ([NSThread isMainThread])
	{
		[self.activity removeFromSuperview];
		[self.activity stopAnimating];
	}
	else
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.activity removeFromSuperview];
			[self.activity stopAnimating];
		});
	}
}

- (void)showActivityBack
{
	if ([NSThread isMainThread])
	{
		[self.activity_back addSubview:self.activity];
		[self.window addSubview:self.activity_back];
		[self.activity startAnimating];
	}
	else
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.activity_back addSubview:self.activity];
			[self.window addSubview:self.activity_back];
			[self.activity startAnimating];
		});
	}
}

- (void)hideActivityBack
{
	if ([NSThread isMainThread])
	{
		[self.activity_back removeFromSuperview];
		[self.activity removeFromSuperview];
		[self.activity stopAnimating];
	}
	else
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.activity_back removeFromSuperview];
			[self.activity removeFromSuperview];
			[self.activity stopAnimating];
		});
	}
}

- (OTActivityIndicator *)activity
{
	if (_activity == nil)
	{
		_activity = [[OTActivityIndicator alloc] initWithFrame:self.window.bounds];
		_activity.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
	}
	
	return _activity;
}

- (UIView *)activity_back
{
	if (_activity_back == nil)
	{
		_activity_back = [[UIView alloc] initWithFrame:self.window.bounds];
		_activity_back.backgroundColor = [UIColor blackColor];
	}
	
	return _activity;
}


@end
