//
//  OTMatrixViewModel.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTMatrixViewModel : NSObject

-(void)generateMatrixOfM:(int)m
					andN:(int)n;

-(BOOL)oneTwoTripDictIsValid;
-(NSString*)getMatrixAsString;
-(NSMutableDictionary*)getOneTwoTripDict;

@end
