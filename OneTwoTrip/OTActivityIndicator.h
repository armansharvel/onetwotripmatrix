//
//  OTActivityIndicator.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTActivityIndicator : UIView

-(void)startAnimating;
-(void)stopAnimating;

@end
