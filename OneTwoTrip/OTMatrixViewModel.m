//
//  OTMatrixViewModel.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTMatrixViewModel.h"
#import "OTMatrixGenerator.h"
#import "OTMatrixAnalyst.h"

@interface OTMatrixViewModel()

@property (nonatomic, strong) OTMatrixGenerator *matrixGenerator;
@property (nonatomic, strong) OTMatrixAnalyst *matrixAnalyst;
@property (nonatomic, strong) NSMutableArray *matrix;
@property (nonatomic, strong) NSMutableDictionary *oneTwoTripDict;

@end

@implementation OTMatrixViewModel

-(void)generateMatrixOfM:(int)m
					andN:(int)n {
	
	self.matrixGenerator = [OTMatrixGenerator new];
	self.matrix = [self.matrixGenerator getMatrixWithM:m
												  andN:n];
	
	self.matrixAnalyst = [OTMatrixAnalyst new];
	self.oneTwoTripDict = [self.matrixAnalyst getFinalOneTwoTripDictFromMatrix:self.matrix];
//	NSLog(@"%@", self.matrix);
//	NSLog(@"%@", self.oneTwoTripDict);
}

-(NSString*)getMatrixAsString {
	NSArray *arrayOfCombinedRows = [self getCombinedMatrixRows];
	NSString *matrixString = [[arrayOfCombinedRows valueForKey:@"description"] componentsJoinedByString:@"\n"];
	return matrixString;
}

-(NSArray*)getCombinedMatrixRows {
	NSMutableArray *mArray = [NSMutableArray new];
	for (int i = 0; i < self.matrix.count; i++) {
		NSString *rowString = [[self.matrix[i] valueForKey:@"description"] componentsJoinedByString:@" "];
		[mArray addObject:rowString];
	}
	return mArray;
}

-(BOOL)oneTwoTripDictIsValid {
	if (self.oneTwoTripDict != nil) {
		return YES;
	}
	return NO;
}

-(NSMutableDictionary*)getOneTwoTripDict {
	return self.oneTwoTripDict;
}


@end
