//
//  NSString+RandomChar.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RandomChar)

+ (NSString *)randomStringFromCharacters:(NSString *)chars;

@end
