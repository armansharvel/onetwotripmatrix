//
//  OTMatrixViewController.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 19/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTMatrixViewController.h"
#import "OTMatrixViewModel.h"
#import "OTOneTwoTripViewController.h"
#import "AppDelegate.h"

@interface OTMatrixViewController () <UITextFieldDelegate>

@property (nonatomic, strong) OTMatrixViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UITextField *mTextField;
@property (weak, nonatomic) IBOutlet UITextField *nTextField;
@property (weak, nonatomic) IBOutlet UILabel *generatedMatrixLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *resultButton;

@property CGFloat minTextViewHeight;
@property BOOL canSegue;

@end

@implementation OTMatrixViewController

#pragma mark - UIViewController LifeCycle

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.viewModel = [OTMatrixViewModel new];
	self.canSegue = NO;
	[self setupTextView];
	[self setupTextFields];
	[self setupDismissKeyboardGesture];
}

#pragma mark - Initialization

-(void)setupTextView {
	self.textViewHeightConstraint.constant = self.minTextViewHeight = self.view.frame.size.height - self.textView.frame.origin.y - 100;
}

-(void)setupTextFields {
	self.mTextField.delegate = self;
	self.nTextField.delegate = self;
}

-(void)setupDismissKeyboardGesture {
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
	tap.cancelsTouchesInView = NO;
	[self.view addGestureRecognizer:tap];
}

#pragma mark - UITextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField {
	textField.text = @"";
	self.generatedMatrixLabel.hidden = YES;
	self.resultButton.hidden = YES;
	[self.resultButton setTitle:@"Generate Matrix" forState:UIControlStateNormal];
	self.canSegue = NO;
	self.textView.hidden = YES;
	self.textViewHeightConstraint.constant = self.minTextViewHeight;
	[self.view layoutIfNeeded];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
	if (self.mTextField.text.length && ![self.mTextField.text containsString:@"M"] &&
		self.nTextField.text.length && ![self.nTextField.text containsString:@"N"]) {
		self.resultButton.hidden = NO;
	} else {
		if (!self.mTextField.text.length || [self.mTextField.text containsString:@"M"]) {
			self.mTextField.text = @"M";
		}
		if (!self.nTextField.text.length || [self.nTextField.text containsString:@"N"]) {
			self.nTextField.text = @"N";
		}
	}
}

#pragma mark - Actions

- (IBAction)resultButtonPressed:(id)sender {
	
	if (self.canSegue) {
		[self performSegueWithIdentifier:@"showOneTwoTrip" sender:nil];
	} else {
		[self.viewModel generateMatrixOfM:[self.mTextField.text intValue]
									 andN:[self.nTextField.text intValue]];
		if (![self.viewModel oneTwoTripDictIsValid]) {
			self.generatedMatrixLabel.text = @"Impossible";
			[self.resultButton setTitle:@"Generate Matrix" forState:UIControlStateNormal];
			self.canSegue = NO;
		} else {
			self.generatedMatrixLabel.text = @"Possible!";
			[self.resultButton setTitle:@"Show OneTwoTrip" forState:UIControlStateNormal];
			self.canSegue = YES;
		}
		self.generatedMatrixLabel.hidden = NO;
		self.textView.text = [self.viewModel getMatrixAsString];
		[self updateTextViewHeigthConstraint];
		self.textView.hidden = NO;
	}
}

#pragma mark - Helper Methods

-(void)dismissKeyboard:(UITapGestureRecognizer*)tap
{
	BOOL tappedTextField = NO;
	
	CGRect mTextFieldBounds = self.mTextField.bounds;
	CGRect nTextFieldBounds = self.nTextField.bounds;
	CGPoint mTapLocation = [tap locationInView:self.mTextField];
	CGPoint nTapLocation = [tap locationInView:self.nTextField];
	
	if ((CGRectContainsPoint(mTextFieldBounds, mTapLocation)) ||
		(CGRectContainsPoint(nTextFieldBounds, nTapLocation))) {
		tappedTextField = YES;
	}
	
	if (!tappedTextField) {
		[self.mTextField resignFirstResponder];
		[self.nTextField resignFirstResponder];
	}
}

-(void)updateTextViewHeigthConstraint {
	CGFloat textViewHeight = [self.textView
							  sizeThatFits:
							  CGSizeMake(self.textView.frame.size.width, CGFLOAT_MAX)].height;
	if (textViewHeight <= self.minTextViewHeight) {
		self.textViewHeightConstraint.constant = self.minTextViewHeight;
	} else {
		self.textViewHeightConstraint.constant = textViewHeight;
	}
	[self.view layoutIfNeeded];
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:@"showOneTwoTrip"]) {
		OTOneTwoTripViewController *dvc = segue.destinationViewController;
		dvc.oneTwoTripDict = [self.viewModel getOneTwoTripDict];
	}
}

@end
