//
//  AppDelegate.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 19/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (AppDelegate *)shared;

- (void)showActivity;
- (void)hideActivity;
- (void)showActivityBack;
- (void)hideActivityBack;

@end

