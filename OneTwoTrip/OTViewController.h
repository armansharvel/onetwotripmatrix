//
//  OTViewController.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 21/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTViewController : UIViewController

-(void)layoutIfNeededWithDuration:(NSTimeInterval)duration;

@end
