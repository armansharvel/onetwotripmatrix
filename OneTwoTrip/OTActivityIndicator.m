//
//  OTActivityIndicator.m
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import "OTActivityIndicator.h"
#import "DGActivityIndicatorView.h"

@interface OTActivityIndicator()

@property (nonatomic, strong) DGActivityIndicatorView *activityIndicatorView;

@end

@implementation OTActivityIndicator

-(id)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	if(self){
		
		UIColor *niceGreen = [UIColor colorWithRed:0.10 green:0.74 blue:0.61 alpha:1.0];
		
		self.activityIndicatorView =  [[DGActivityIndicatorView alloc]
									   initWithType:DGActivityIndicatorAnimationTypeCookieTerminator
									   tintColor:niceGreen];
		
		CGFloat width = 60;
		CGFloat height = 100;
		self.activityIndicatorView.frame = CGRectMake(self.frame.size.width/2 - width/2,
													  self.frame.size.height/2 - height/2,
													  width,
													  height);
		
		[self addSubview:self.activityIndicatorView];
	}
	return self;
}

-(void)startAnimating{
	[self.activityIndicatorView startAnimating];
}

-(void)stopAnimating{
	[self.activityIndicatorView stopAnimating];
}

@end
