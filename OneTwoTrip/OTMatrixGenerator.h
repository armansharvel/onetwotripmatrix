//
//  OTGenerateMatrix.h
//  OneTwoTrip
//
//  Created by Arman Arutyunov on 20/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTMatrixGenerator : NSObject

-(NSMutableArray*)getMatrixWithM:(int)m
							andN:(int)n;

@end
